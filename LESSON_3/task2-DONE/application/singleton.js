/*
  Задание:

    Написать синглтон, который будет создавать обьект government

    Данные:
    {
        laws: [],
        budget: 1000000
        citizensSatisfactions: 0,
    }

    У этого обьекта будут методы:
      .добавитьЗакон({id, name, description})
        -> добавляет закон в laws и понижает удовлетворенность граждан на -10

      .читатькКонституцию -> Вывести все законы на экран
      .читатьЗакон(ид)

      .показатьУровеньДовольства()
      .показатьБюджет()
      .провестиПраздник -> отнимает от бюджета 50000, повышает удовлетворенность граждан на +5


*/



//**************************************** OPEN INDEX.HTML IN BROWSER

let wrapper = document.getElementById('data');

const startData = {
        laws: [],
        budget: 1000000,
        citizensSatisfaction: 0
};

const Government = {
        addLaw: (id, name, description) => {
                wrapper.innerHTML = '';
                if (id !== '' && name !== '' && description !== '') {
                        function Law(id, name, description) {
                                this.id = id;
                                this.name = name;
                                this.description = description;

                                startData.laws.push(this);
                                startData.citizensSatisfaction = startData.citizensSatisfaction - 10;
                        }

                        let law = new Law(id, name, description);

                        wrapper.innerHTML += 'Your law was added. Citizens Satisfaction - 10'
                } else {
                        wrapper.innerHTML += 'Your have to fill all inputs'
                }

        },

        readConstitution: () => {
                wrapper.innerHTML = '';
                if (startData.laws.length > 0) {
                        for (let i = 0; i < startData.laws.length; i++) {
                                wrapper.innerHTML += `<p>Law ID ${startData.laws[i].id}  <span class="bold">${startData.laws[i].name}</span> : ${startData.laws[i].description}</p>`;
                        }
                } else {
                        wrapper.innerHTML += 'Your Government don\'t have laws yet';
                }

        },

        readLaw: id => {
                wrapper.innerHTML = '';
                let Law = startData.laws.find(l => l.id === id);
                if (Law !== undefined) {
                        wrapper.innerHTML += `<p>Law ID ${Law.id}  <span class="bold">${Law.name}</span> : ${Law.description}</p>`;
                } else {
                        wrapper.innerHTML += 'Your Government don\'t have laws with current ID'
                }
        },

        showCitizensSatisfaction: () => {
                wrapper.innerHTML = '';
                wrapper.innerHTML += startData.citizensSatisfaction;
        },

        showBudget: () => {
                wrapper.innerHTML = '';
                wrapper.innerHTML += startData.budget;
        },

        runEvent: () => {
                wrapper.innerHTML = '';
                wrapper.innerHTML = 'You have party!!!!!';

                startData.budget = startData.budget - 50000;
                startData.citizensSatisfaction = startData.citizensSatisfaction + 5;
        },
};

let addLaw = document.getElementById('addLaw');
addLaw.addEventListener('click', () => {
        let lawId = document.getElementById('id').value;
        let lawName = document.getElementById('name').value;
        let lawDescription = document.getElementById('description').value;
        Government.addLaw(lawId, lawName, lawDescription);
        lawId.value = '';
        lawName.value = '';
        lawDescription.value = '';
});

let readConstitution = document.getElementById('readConstitution');
readConstitution.addEventListener('click', Government.readConstitution);

let readLaw = document.getElementById('readLaw');
readLaw.addEventListener('click', () => {
        let lawIdRead = document.getElementById('idGet').value;
        Government.readLaw(lawIdRead)
});

let showCitizensSatisfaction = document.getElementById('showCitizensSatisfaction');
showCitizensSatisfaction.addEventListener('click', Government.showCitizensSatisfaction);

let showBudget = document.getElementById('showBudget');
showBudget.addEventListener('click', Government.showBudget);

let runEvent = document.getElementById('runEvent');
runEvent.addEventListener('click', Government.runEvent);

Government.readConstitution();