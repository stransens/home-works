/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/singleton.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/singleton.js":
/*!**********************************!*\
  !*** ./application/singleton.js ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("/*\n  Задание:\n\n    Написать синглтон, который будет создавать обьект government\n\n    Данные:\n    {\n        laws: [],\n        budget: 1000000\n        citizensSatisfactions: 0,\n    }\n\n    У этого обьекта будут методы:\n      .добавитьЗакон({id, name, description})\n        -> добавляет закон в laws и понижает удовлетворенность граждан на -10\n\n      .читатькКонституцию -> Вывести все законы на экран\n      .читатьЗакон(ид)\n\n      .показатьУровеньДовольства()\n      .показатьБюджет()\n      .провестиПраздник -> отнимает от бюджета 50000, повышает удовлетворенность граждан на +5\n\n\n*/\n\n\n\n//**************************************** OPEN INDEX.HTML IN BROWSER\n\nlet wrapper = document.getElementById('data');\n\nconst startData = {\n        laws: [],\n        budget: 1000000,\n        citizensSatisfaction: 0\n};\n\nconst Government = {\n        addLaw: (id, name, description) => {\n                wrapper.innerHTML = '';\n                if (id !== '' && name !== '' && description !== '') {\n                        function Law(id, name, description) {\n                                this.id = id;\n                                this.name = name;\n                                this.description = description;\n\n                                startData.laws.push(this);\n                                startData.citizensSatisfaction = startData.citizensSatisfaction - 10;\n                        }\n\n                        let law = new Law(id, name, description);\n\n                        wrapper.innerHTML += 'Your law was added. Citizens Satisfaction - 10'\n                } else {\n                        wrapper.innerHTML += 'Your have to fill all inputs'\n                }\n\n        },\n\n        readConstitution: () => {\n                wrapper.innerHTML = '';\n                if (startData.laws.length > 0) {\n                        for (let i = 0; i < startData.laws.length; i++) {\n                                wrapper.innerHTML += `<p>Law ID ${startData.laws[i].id}  <span class=\"bold\">${startData.laws[i].name}</span> : ${startData.laws[i].description}</p>`;\n                        }\n                } else {\n                        wrapper.innerHTML += 'Your Government don\\'t have laws yet';\n                }\n\n        },\n\n        readLaw: id => {\n                wrapper.innerHTML = '';\n                let Law = startData.laws.find(l => l.id === id);\n                if (Law !== undefined) {\n                        wrapper.innerHTML += `<p>Law ID ${Law.id}  <span class=\"bold\">${Law.name}</span> : ${Law.description}</p>`;\n                } else {\n                        wrapper.innerHTML += 'Your Government don\\'t have laws with current ID'\n                }\n        },\n\n        showCitizensSatisfaction: () => {\n                wrapper.innerHTML = '';\n                wrapper.innerHTML += startData.citizensSatisfaction;\n        },\n\n        showBudget: () => {\n                wrapper.innerHTML = '';\n                wrapper.innerHTML += startData.budget;\n        },\n\n        runEvent: () => {\n                wrapper.innerHTML = '';\n                wrapper.innerHTML = 'You have party!!!!!';\n\n                startData.budget = startData.budget - 50000;\n                startData.citizensSatisfaction = startData.citizensSatisfaction + 5;\n        },\n};\n\nlet addLaw = document.getElementById('addLaw');\naddLaw.addEventListener('click', () => {\n        let lawId = document.getElementById('id').value;\n        let lawName = document.getElementById('name').value;\n        let lawDescription = document.getElementById('description').value;\n        Government.addLaw(lawId, lawName, lawDescription);\n        lawId.value = '';\n        lawName.value = '';\n        lawDescription.value = '';\n});\n\nlet readConstitution = document.getElementById('readConstitution');\nreadConstitution.addEventListener('click', Government.readConstitution);\n\nlet readLaw = document.getElementById('readLaw');\nreadLaw.addEventListener('click', () => {\n        let lawIdRead = document.getElementById('idGet').value;\n        Government.readLaw(lawIdRead)\n});\n\nlet showCitizensSatisfaction = document.getElementById('showCitizensSatisfaction');\nshowCitizensSatisfaction.addEventListener('click', Government.showCitizensSatisfaction);\n\nlet showBudget = document.getElementById('showBudget');\nshowBudget.addEventListener('click', Government.showBudget);\n\nlet runEvent = document.getElementById('runEvent');\nrunEvent.addEventListener('click', Government.runEvent);\n\nGovernment.readConstitution();\n\n//# sourceURL=webpack:///./application/singleton.js?");

/***/ })

/******/ });