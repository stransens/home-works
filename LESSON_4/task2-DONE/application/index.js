/*

  Используя методы для создания объектов из задания по композиции написать фабику (HeadHunt), которая будет
  расспределять и создавать сотрудников компании нужного типа.

  Данные для списка сотрудников: http://www.json-generator.com/api/json/get/cfeTmcNIXS?indent=2

  HeadHunt => {
    hire( obj ){
      ...
    }
  }

  Привязать к интерфейсу и вывести на страницу. На кнопку нанять повесить метод hire из фабрики

*/

import {BackendDeveloper, FrontendDeveloper, Designer, ProjectManager} from '../../task1/application/index';

let renderTeam = document.getElementById('your');

const fabric = list => {
        let arrayHired = [];

        function render() {
                const table = document.querySelector('table');
                list.map(person => {
                        let worker = HeadHuntFactory.hire(person);
                        let tr = document.createElement('tr');
                        tr.innerHTML = `
                                <td>
                                    ${person.name} (${person.age})
                                </td>                
                                <td>
                                    ${person.type}
                                </td>        
                                <td>
                                    <button data-id="${person.name}">Hire</button>
                                </td>
                                `
                        table.appendChild(tr);
                        const btn = table.querySelector(`button[data-id="${person.name}"]`);
                        btn.addEventListener('click', hire);

                        function hire() {
                                let idx = list.indexOf(person);
                                const table = document.createElement('table');
                                list.splice(idx, 1);
                                arrayHired.push(person);
                                arrayHired.map(person => {
                                        table.innerHTML = `
                                                <tr>
                                                  <td>${person.name}</td>
                                                  <td>${person.type}</td>
                                                  <td>${person.rate} $/hour</td>
                                                </tr>
                                          `
                                });
                                renderTeam.appendChild(table);
                        }
                });
        }


        class HeadHunt {
                hire(candidat) {
                        let Employee = null;
                        if (candidat.type === "backend") {
                                Employee = BackendDeveloper(candidat.name, candidat.age, candidat.gender, candidat.rate);
                        } else if (candidat.type === "frontend") {
                                Employee = FrontendDeveloper(candidat.name, candidat.age, candidat.gender, candidat.rate);
                        } else if (candidat.type === "design") {
                                Employee = Designer(candidat.name, candidat.age, candidat.gender, candidat.rate);
                        } else if (candidat.type === "project") {
                                Employee = ProjectManager(candidat.name, candidat.age, candidat.gender, candidat.rate);
                        } else {
                                return false;
                        }
                        return Employee;
                }
        }

        let HeadHuntFactory = new HeadHunt();

        render();
};

let listOfCandidats = fetch('http://www.json-generator.com/api/json/get/cfeTmcNIXS?indent=2')
        .then(res => res.json())
        .then(fabric);





