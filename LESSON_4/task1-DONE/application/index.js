/*
  Composition:

  Задание при помощи композиции создать объекты 4х типов:

  functions:
    - MakeBackendMagic
    - MakeFrontendMagic
    - MakeItLooksBeautiful
    - DistributeTasks
    - DrinkSomeTea
    - WatchYoutube
    - Procrastinate

  BackendDeveloper = MakeBackendMagic + DrinkSomeTea + Procrastinate
  FrontendDeveloper = MakeFrontendMagic + DrinkSomeTea + WatchYoutube
  Designer = MakeItLooksBeautiful + WatchYoutube + Procrastinate
  ProjectManager = DistributeTasks + Procrastinate + DrinkSomeTea

  ProjectManager(name,gender,age) => { name, gender, age, type: 'project', rate: '15/h'}

*/

const MakeBackendMagic = (state) => ({
        makeBackendMagic: () => console.log(state.name + ' is' + state.type + ' so he can make makeBackendMagic!')
});

const MakeFrontendMagic = (state) => ({
        makeFrontendMagic: () => console.log(state.name + ' is' + state.type + ' so he can make makeFrontendMagic!')
});

const MakeItLooksBeautiful = (state) => ({
        makeItLooksBeautiful: () => console.log(state.name + ' is' + state.type + ' so he can make makeItLooksBeautiful!')
});

const DistributeTasks = (state) => ({
        distributeTasks: () => console.log(state.name + ' is' + state.type + ' so he can make distributeTasks!')
});

const DrinkSomeTea = (state) => ({
        drinkSomeTea: () => console.log(state.name + ' is' + state.type + ' so he can make drinkSomeTea!')
});

const WatchYoutube = (state) => ({
        watchYoutube: () => console.log(state.name + ' is' + state.type + ' so he can make watchYoutube!')
});

const Procrastinate = (state) => ({
        procrastinate: () => console.log(state.name + ' is' + state.type + ' so he can make procrastinate!')
});

const Render = (state) => ({
        render: () => {
                let tr = document.createElement('tr');
                const template =
                        `<td>${state.name} (${state.age}) </td><td>${state.type}</td><td><button id="button_${state.id}">Hire!</button></td>`
                tr.innerHTML = template;
                let renderAvailable = document.getElementById('available');
                renderAvailable.appendChild(tr);

                let button = document.getElementById(`button_${state.id}`);
        }
});

export const BackendDeveloper = (name, gender, age, _id) => {
        let state = {
                name: name,
                gender: gender,
                age: age,
                type: 'backend',
                id: _id
        };
        return Object.assign(
                {},
                state,
                MakeBackendMagic(state),
                DrinkSomeTea(state),
                Procrastinate(state),
                Render(state)
        );
};

export const FrontendDeveloper = (name, gender, age, _id) => {
        let state = {
                name: name,
                gender: gender,
                age: age,
                type: 'frontend',
                id: _id
        };
        return Object.assign(
                {},
                state,
                MakeFrontendMagic(state),
                DrinkSomeTea(state),
                WatchYoutube(state),
                Render(state),
        );
};

export const Designer = (name, gender, age, _id) => {
        let state = {
                name: name,
                gender: gender,
                age: age,
                type: 'designer',
                id: _id
        };
        return Object.assign(
                {},
                state,
                MakeItLooksBeautiful(state),
                WatchYoutube(state),
                Procrastinate(state),
                Render(state)
        );
};

export const ProjectManager = (name, gender, age, _id) => {
        let state = {
                name: name,
                gender: gender,
                age: age,
                type: 'project',
                id: _id
        };
        return Object.assign(
                {},
                state,
                DistributeTasks(state),
                Procrastinate(state),
                DrinkSomeTea(state),
                Render(state)
        );
};

const VladB = BackendDeveloper('Vlad', 'man', 23);
console.log(VladB);

const SashaF = Designer('Sasha', 'girl', 24);
console.log(SashaF);

const SerhiiO = ProjectManager('Serhii', 'man', 33);
console.log(SerhiiO);

const NadiaV = FrontendDeveloper('NadiaV', 'girl', 29);
console.log(NadiaV);

