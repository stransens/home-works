/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/index.js":
/*!******************************!*\
  !*** ./application/index.js ***!
  \******************************/
/*! exports provided: BackendDeveloper, FrontendDeveloper, Designer, ProjectManager */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"BackendDeveloper\", function() { return BackendDeveloper; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"FrontendDeveloper\", function() { return FrontendDeveloper; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Designer\", function() { return Designer; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"ProjectManager\", function() { return ProjectManager; });\n/*\n  Composition:\n\n  Задание при помощи композиции создать объекты 4х типов:\n\n  functions:\n    - MakeBackendMagic\n    - MakeFrontendMagic\n    - MakeItLooksBeautiful\n    - DistributeTasks\n    - DrinkSomeTea\n    - WatchYoutube\n    - Procrastinate\n\n  BackendDeveloper = MakeBackendMagic + DrinkSomeTea + Procrastinate\n  FrontendDeveloper = MakeFrontendMagic + DrinkSomeTea + WatchYoutube\n  Designer = MakeItLooksBeautiful + WatchYoutube + Procrastinate\n  ProjectManager = DistributeTasks + Procrastinate + DrinkSomeTea\n\n  ProjectManager(name,gender,age) => { name, gender, age, type: 'project', rate: '15/h'}\n\n*/\n\nconst MakeBackendMagic = (state) => ({\n        makeBackendMagic: () => console.log(state.name + ' is' + state.type + ' so he can make makeBackendMagic!')\n});\n\nconst MakeFrontendMagic = (state) => ({\n        makeFrontendMagic: () => console.log(state.name + ' is' + state.type + ' so he can make makeFrontendMagic!')\n});\n\nconst MakeItLooksBeautiful = (state) => ({\n        makeItLooksBeautiful: () => console.log(state.name + ' is' + state.type + ' so he can make makeItLooksBeautiful!')\n});\n\nconst DistributeTasks = (state) => ({\n        distributeTasks: () => console.log(state.name + ' is' + state.type + ' so he can make distributeTasks!')\n});\n\nconst DrinkSomeTea = (state) => ({\n        drinkSomeTea: () => console.log(state.name + ' is' + state.type + ' so he can make drinkSomeTea!')\n});\n\nconst WatchYoutube = (state) => ({\n        watchYoutube: () => console.log(state.name + ' is' + state.type + ' so he can make watchYoutube!')\n});\n\nconst Procrastinate = (state) => ({\n        procrastinate: () => console.log(state.name + ' is' + state.type + ' so he can make procrastinate!')\n});\n\nconst Render = (state) => ({\n        render: () => {\n                let tr = document.createElement('tr');\n                const template =\n                        `<td>${state.name} (${state.age}) </td><td>${state.type}</td><td><button id=\"button_${state.id}\">Hire!</button></td>`\n                tr.innerHTML = template;\n                let renderAvailable = document.getElementById('available');\n                renderAvailable.appendChild(tr);\n\n                let button = document.getElementById(`button_${state.id}`);\n        }\n});\n\nconst BackendDeveloper = (name, gender, age, _id) => {\n        let state = {\n                name: name,\n                gender: gender,\n                age: age,\n                type: 'backend',\n                id: _id\n        };\n        return Object.assign(\n                {},\n                state,\n                MakeBackendMagic(state),\n                DrinkSomeTea(state),\n                Procrastinate(state),\n                Render(state)\n        );\n};\n\nconst FrontendDeveloper = (name, gender, age, _id) => {\n        let state = {\n                name: name,\n                gender: gender,\n                age: age,\n                type: 'frontend',\n                id: _id\n        };\n        return Object.assign(\n                {},\n                state,\n                MakeFrontendMagic(state),\n                DrinkSomeTea(state),\n                WatchYoutube(state),\n                Render(state),\n        );\n};\n\nconst Designer = (name, gender, age, _id) => {\n        let state = {\n                name: name,\n                gender: gender,\n                age: age,\n                type: 'designer',\n                id: _id\n        };\n        return Object.assign(\n                {},\n                state,\n                MakeItLooksBeautiful(state),\n                WatchYoutube(state),\n                Procrastinate(state),\n                Render(state)\n        );\n};\n\nconst ProjectManager = (name, gender, age, _id) => {\n        let state = {\n                name: name,\n                gender: gender,\n                age: age,\n                type: 'project',\n                id: _id\n        };\n        return Object.assign(\n                {},\n                state,\n                DistributeTasks(state),\n                Procrastinate(state),\n                DrinkSomeTea(state),\n                Render(state)\n        );\n};\n\nconst VladB = BackendDeveloper('Vlad', 'man', 23);\nconsole.log(VladB);\n\nconst SashaF = Designer('Sasha', 'girl', 24);\nconsole.log(SashaF);\n\nconst SerhiiO = ProjectManager('Serhii', 'man', 33);\nconsole.log(SerhiiO);\n\nconst NadiaV = FrontendDeveloper('NadiaV', 'girl', 29);\nconsole.log(NadiaV);\n\n\n\n//# sourceURL=webpack:///./application/index.js?");

/***/ })

/******/ });