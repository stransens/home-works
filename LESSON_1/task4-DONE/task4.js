/*

  Данные: http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2
  Задача.

  1.  Получить данные и в виде простой таблички вывести список компаний. Для начала используем поля:
      Company | Balance | Registered | Показать адресс | Кол-во employers | показать сотрудников

  2.  Сделать сортировку таблицы по количеству сотрудников и балансу. Сортировка должна происходить по клику
      на заголовок столбца

  3.  По клику на показать адресс должна собиратся строка из полей адресса и показываться на экран.

  4.  По клику на показать сотрудников должна показываться другая табличка формата:
      <- Назад к списку компаний | *Название компании*
      Сотрудники:
      Name | Gender | Age | Contacts

  5.  В второй табличке долен быть реализован поиск сотрудников по их имени, а так же сортировка по
      полу и возрасту.

  Примечание: Весь код должен писатся с учетом синтаксиса и возмжность ES6.

*/

function httpGet(theUrl) {
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.open("GET", theUrl, false); // false for synchronous request
        xmlHttp.send(null);
        var data = xmlHttp.responseText;
        var jsonResponse = JSON.parse(data);
        var amount = jsonResponse;
        console.log(amount);
        let usersBlock = document.querySelector('#users');
        return amount;
}

var theUrl = 'http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2';

var array = httpGet(theUrl);

function renderHead(content) {
        let tableHead = document.getElementById('table-head');
        tableHead.innerHTML = content;
}

let templateMainHeader = ` <tr>
            <td>Company</td>
            <td id="balance">Balance</td>
            <td>Registered</td>
            <td>Address</td>
            <td>Кол-во employers</td>
            <td>Employers</td>
      </tr>`;


renderHead(templateMainHeader);

class Company {
        constructor(obj) {
                this.address = obj.address;
                this.balance = obj.balance;
                this.company = obj.company;
                this.employers = obj.employers;
                this.registered = obj.registered;
                this.id = obj._id;

                this.renderAddress = this.renderAddress.bind(this);
                this.renderList = this.renderList.bind(this);
                this.renderListHeader = this.renderListHeader.bind(this);
                this.sortGender = this.sortGender.bind(this);
                this.sortAge = this.sortAge.bind(this);
        }

        renderAddress() {
                let addressObject = this.address;
                let addressString = [];
                for (var i in addressObject) {
                        addressString.push(addressObject[i])
                }
                alert(addressString.join(', '));

        }

        employeString(employers) {
                let {name, gender, age, phones, emails} = employers;
                let tr = document.createElement('tr');
                const template =
                        `
                                    <td>${name}</td>
                                    <td>${gender}</td>
                                    <td>${age}</td>
                                    <td id="contacts-td"></td>
                          
                                `;
                tr.innerHTML = template;
                tr.querySelector('#contacts-td').appendChild(this.renderContacts(phones));
                tr.querySelector('#contacts-td').appendChild(this.renderEmails(emails));

                let renderArea = document.getElementById('table-body');
                renderArea.appendChild(tr);
        }

        renderContacts(phones) {
                let ulContacts = document.createElement('ul');
                for (let i = 0; i < phones.length; i++) {
                        let li = document.createElement('li');
                        let string = `<span>${phones[i]}</span>`;
                        li.innerHTML = string;
                        ulContacts.append(li);
                }
                return ulContacts;
        }

        renderEmails(emails) {
                let ulContacts = document.createElement('ul');
                for (let i = 0; i < emails.length; i++) {
                        let li = document.createElement('li');
                        let string = `<span>${emails[i]}</span>`;
                        li.innerHTML = string;
                        ulContacts.append(li);
                }
                return ulContacts;
        }

        renderListHeader(employers) {
                let templateListHeader = ` <tr>
                                            <td>Name</td>
                                            <td id="sortGender">Gender</td>
                                            <td id="sortAge">Age</td>
                                            <td>Contacts</td>
                                      </tr>`
                renderHead(templateListHeader);
                let genderSort = document.getElementById('sortGender');
                genderSort.addEventListener('click', () => this.sortGender(employers));

                let ageSort = document.getElementById('sortAge');
                ageSort.addEventListener('click', () => this.sortAge(employers));
        }

        renderListControls(employers) {
                let templateListHeader = ` <tr>
                                            <td id="backMainTable">Вернуться к списку компаний</td>
                                            <td> - </td>
                                            <td>${this.company}</td>
                                      </tr>`;

                let controlsBlock = document.getElementById('controls');
                controlsBlock.innerHTML = templateListHeader;

                let genderSort = document.getElementById('backMainTable');
                genderSort.addEventListener('click', () => {
                        let searchBlock = document.getElementById('searchBlock');
                        searchBlock.className = '';
                        let renderArea = document.getElementById('table-body');
                        renderArea.innerHTML = '';
                        let controlsBlock = document.getElementById('controls');
                        controlsBlock.innerHTML = '';
                        renderMainTable();
                });
        }

        sortGender(employers) {
                let renderArea = document.getElementById('table-body');
                renderArea.innerHTML = '';

                let genderArray = employers.sort(function (a, b) {
                        if (a.gender < b.gender) {
                                console.log(a, b);
                                return -1;
                        }
                        if (a.gender > b.gender) {
                                console.log(a, b);
                                return 1;
                        }
                        return 0;
                });
                this.renderList(genderArray);
        }

        sortAge(employers) {
                let renderArea = document.getElementById('table-body');
                renderArea.innerHTML = '';

                let ageArray = employers.sort(function (a, b) {
                        if (a.age < b.age) {
                                console.log(a, b);
                                return -1;
                        }
                        if (a.age > b.age) {
                                console.log(a, b);
                                return 1;
                        }
                        return 0;
                });
                this.renderList(ageArray);
        }

        search(employers) {
                let value = document.getElementById('input').value;

                let searchArray = [];
                for (let i = 0; i < employers.length; i++) {
                        if (employers[i].name.match(value)) {
                                searchArray.push(employers[i]);
                        }
                }
                this.renderList(searchArray);
        }

        renderList(employers) {
                let searchBlock = document.getElementById('searchBlock');
                searchBlock.className = 'visible';
                search.addEventListener('click', () => this.search(employers));
                let renderArea = document.getElementById('table-body');
                renderArea.innerHTML = '';
                for (let i = 0; i < employers.length; i++) {
                        this.employeString(employers[i]);
                }
        }

        render() {
                let _this = this;
                let tr = document.createElement('tr');
                tr.dataset.id = this.id;
                const template =
                        `
                                    <td>${this.company}</td>
                                    <td>${this.balance}</td>
                                    <td>${this.registered}</td>
                                    <td><button id="button_${this.id}">Show address</button></td>
                                    <td>${this.employers.length}</td>
                                    <td><button id="button2_${this.id}">Show employers</button></td>
                                `
                tr.innerHTML = template;
                let renderArea = document.getElementById('table-body');
                renderArea.appendChild(tr);

                let button = document.getElementById(`button_${this.id}`);
                button.addEventListener('click', _this.renderAddress)

                let button2 = document.getElementById(`button2_${this.id}`);
                button2.addEventListener('click', () => {
                        _this.renderList(this.employers); _this.renderListHeader(this.employers); _this.renderListControls()
                })
        }
}

const myArr = [];

let renderMainTable = () => {
        for (let i = 0; i < array.length; i++) {
                let item = new Company(array[i]);
                item.render();
                myArr.push(item)
        }
}


let balanceSort = document.getElementById('balance');
balanceSort.addEventListener('click', sortBalance);

function sortBalance() {
        let renderArea = document.getElementById('table-body');
        renderArea.innerHTML = '';
        let sortedArray = myArr.sort(function (a, b) {
                if (a.balance < b.balance) {
                        console.log(a.balance, b.balance);
                        return -1;
                }
                if (a.balance > b.balance) {
                        console.log(a.balance, b.balance);
                        return 1;
                }
                return 0;
        });
        console.log(sortedArray);
        for (let i = 0; i < sortedArray.length; i++) {
                sortedArray[i].render();
        }
}

renderMainTable();




