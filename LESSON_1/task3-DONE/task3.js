/*
  Задание:
  1. При помощи методов изложеных в arrays.js , переформатировать ITEA_COURSES в массив который содержит длину строк каждого из элементов.
  2. Самостоятельно изучить метод Array.sort. Отфильтровать массив ITEA_COURSES по алфавиту.
      + Бонусный бал. Вывести на страничку списком
  3. Реализация функции поиска по массиву ITEA_COURSES.
      + Бонусный бал. Вывести на страничку инпут и кнопку по которой будет срабатывать поиск.

*/

const ITEA_COURSES = ["Курс HTML & CSS", "JavaScript базовый курс", "JavaScript продвинутый курс", "JavaScript Professional", "Angular 2.4 (базовый)", "Angular 2.4 (продвинутый)", "React.js", "React Native", "Node.js", "Vue.js"];

let renderArea = document.getElementById('renderArea');

//array with item's length
let length = document.querySelector('#length');
length.addEventListener('click', renderList);

function renderList() {
        let newArray = ITEA_COURSES.map((item, i) => item.length);

        renderArea.innerHTML = '';

        newArray.map(item => {
                let li = lengthList(item);
        })
}

function lengthList(item) {
        let div = document.createElement('li');
        const template = `<p>${item}</p>`;
        div.innerHTML = template;
        renderArea.appendChild(div);
}


//array with sort
let sort = document.querySelector('#sort');
sort.addEventListener('click', renderList2);

function renderList2() {
        let newArray2 = ITEA_COURSES.sort(function (a, b) {
                if (a < b) {
                        return -1;
                }
                if (a > b) {
                        return 1;
                }
                return 0;
        });

        renderArea.innerHTML = '';

        newArray2.map(item => {
                let li = sortList(item);
        })
}

function sortList(item) {
        let div = document.createElement('li');
        const template = `<p>${item}</p>`;
        div.innerHTML = template;
        renderArea.appendChild(div);
}


//search
let search = document.querySelector('#search');
search.addEventListener('click', renderList3);

function renderList3() {
        let value = document.getElementById('input').value;

        renderArea.innerHTML = '';

        let newArray3 = [];
        for (var i = 0; i < ITEA_COURSES.length; i++) {
                if (ITEA_COURSES[i].match(value)) {
                        newArray3.push(ITEA_COURSES[i]);
                };
        }
        newArray3.map(item => searchList(item))
}

function searchList(item) {
        let div = document.createElement('li');
        const template = `<p>${item}</p>`;
        div.innerHTML = template;
        renderArea.appendChild(div);
}