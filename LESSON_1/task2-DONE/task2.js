/*

         Задание 2.

         Проверка уровня знаний для старта.

         1) Ваша задача используя классы или ф-ю конструктор, создать обьекты этого класса
         которые будут содержать все данные которые есть в обьекте ниже (переменная data),
         а так же будут содержать дополнительное поле likes, которое по умолчанию будет равно нулю.

         2) Дальше, вам необходимо вывести эти сообщения на страницу по шаблону
         (макет находится в папке classworks).

         3) Кнопка like должна обновлять счетчик лайков в этом обьекте и перерисовывать обьект
         на странице

         + бонус:
           - Хранить данные о лайках постов в localStorage и не давать пользователю за один
           заход на страничку поставить больше одного лайка на один пост.
           - После перезагрузки страничка подтягивает данные из localStorage о количестве
           лайков у каждого сообщения

           - Добавить к классу\конструктору поле "answers" (В модель), а в шаблон вывода
           под сообщением инпут в который можно будет добавить комментарий и кнопку для его
           отправки (Макет: js_prof_l1c2_bonus)
           - Комментарии так же хранятся в localStorage и подгружаются после перезагруки
           страницы.

       */

const data = [
        {
                id: 1,
                link: "#1",
                name: "Established fact123123",
                description: "The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. ",
                image: "http://telegram.org.ru/uploads/posts/2017-10/1507400926_file_162303.jpg"
        },
        {
                id: 2,
                link: "#1",
                name: "Established fact",
                description: "The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. ",
                image: "http://telegram.org.ru/uploads/posts/2017-10/1507400926_file_162303.jpg"
        },
        {
                id: 3,
                link: "#2",
                name: "Many packages",
                description: "Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy.",
                image: "http://telegram.org.ru/uploads/posts/2017-10/1507400859_file_162309.jpg"
        },
        {
                id: 4,
                link: "#3",
                name: "Suffered alteration",
                description: "Looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature.",
                image: "http://telegram.org.ru/uploads/posts/2017-10/1507400896_file_162315.jpg"
        }, {
                id: 5,
                link: "#4",
                name: "Discovered source",
                description: "It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged",
                image: "http://telegram.org.ru/uploads/posts/2017-10/1507400878_file_162324.jpg"
        }, {
                id: 6,
                link: "#5",
                name: "Handful model",
                description: "The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.",
                image: "http://telegram.org.ru/uploads/posts/2017-10/1507400876_file_162328.jpg"
        },
];

class Post {
        constructor(post) {
                this.id = post.id;
                this.link = post.link;
                this.name = post.name;
                this.description = post.description;
                this.image = post.image;
                this.likes = 0;
                this.session = true;
                this.nameOfStoreLikes = 'store' + this.id + 'likes';
                this.nameOfStoreComments = 'store' + this.id + 'comments';
                this.addLike = this.addLike.bind(this);
        }

        getStorageInfo() {
                let likes = localStorage.getItem(this.nameOfStoreLikes);
                this.likes = likes != null ? likes : 0;
                let node = document.querySelector(`.post-block[data-id="${this.id}"]`);
                let span = node.querySelector('span');
                span.innerText = `${this.likes}`;
        }

        addLike() {
                if (this.session) {
                        this.likes++;
                        localStorage.setItem(this.nameOfStoreLikes, this.likes);
                        let node = document.querySelector(`.post-block[data-id="${this.id}"]`);
                        let span = node.querySelector('span');
                        span.innerText = `${this.likes}`;
                        this.session = false;
                } else {
                        alert('Вы уже оценивали в этой сессии!!')
                }
        }

        addComment() {
                let comment = document.getElementById(`input${this.id}`);
                let commentText = comment.value;
                comment.value = '';
                let comments = JSON.parse(localStorage.getItem(this.nameOfStoreComments));
                if (comments != null) {
                        comments.push(commentText);
                        localStorage.setItem(this.nameOfStoreComments, JSON.stringify(comments));
                        this.renderComments();
                } else {
                        let comments = [];
                        comments.push(commentText);
                        localStorage.setItem(this.nameOfStoreComments, JSON.stringify(comments));
                        this.renderComments();
                }
        }

        renderComments() {
                let comments = JSON.parse(localStorage.getItem(this.nameOfStoreComments));
                let node = document.querySelector(`.post-block[data-id="${this.id}"]`);
                let blockForComments = node.querySelector('.comments-block');
                let blockForCommentsContent = node.querySelector('.comments-content');
                let CommentsCounter = node.querySelector('.comments-counter');

                blockForCommentsContent.innerHTML = '';
                console.log(typeof comments);
                if (comments != null) {
                        blockForComments.className = 'comments-block';
                        CommentsCounter.innerHTML = '(' + comments.length + ')';
                        return comments.map((comment, i) => blockForCommentsContent.innerHTML += `<p><span>${i + 1 + '.  '}</span>${comment}</p>`)
                } else {

                }
        }

        render() {
                let div = document.createElement('div');
                div.dataset.id = this.id;
                div.className = 'post-block';
                const template =
                        `
                                    <div class="image-block">
                                          <img src="${this.image}" alt='' />
                                    </div>
                                    <div>
                                          <h3>${this.name}</h3>
                                          <p>${this.description}</p>
                                          <div class="likes-block">
                                                <button id="like${this.id}"><i class="fas fa-heart"></i></button>
                                                <span class="likesCounter${this.id}">${this.likes}</span>
                                          </div>
                                          <div class="comments-block hide">
                                                <p>Comments <span class="comments-counter">0</span></p>
                                                <div class="comments-content"></div>
                                          </div>
                                          <div>
                                                <input id="input${this.id}" type="text" placeholder="Write your comment">
                                                <button id="comment${this.id}">Send</button>
                                          </div>
                                    </div>
                                `;
                div.innerHTML = template;
                let renderArea = document.getElementById('renderArea');
                renderArea.appendChild(div);

                let likeButton = document.getElementById('like' + this.id);
                likeButton.addEventListener('click', () => {
                        likeButton.className = 'done';
                        this.addLike(this.id)
                });

                let commentButton = document.getElementById('comment' + this.id);
                commentButton.addEventListener('click', () => this.addComment(this.id))
        }
}

data.map((post, i) => {
        let newPost = new Post(post);
        newPost.render();
        newPost.getStorageInfo();
        newPost.renderComments();
});