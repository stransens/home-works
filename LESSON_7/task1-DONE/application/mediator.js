/*
  Написать медиатор для группы студентов.

  Профессор отвечает только на вопросы старосты.

  У Студента есть имя и группа которой он принадлежит.
  Он может запросить старосту задать вопрос или получить ответ.

  Староста может добавлять студентов в группу и передавать вопрос профессору.
*/



//**************************************** OPEN INDEX.HTML IN BROWSER

const chat = document.getElementById('chat');
const info = document.getElementById('info');
const list = document.getElementById('list');
const Mediator = () => {

        class Professor {
                constructor(name) {
                        this.name = name;
                        this.type = 'professor';

                        info.innerHTML += (`<p>New professor <span class="bold"> ${this.name} </span>! Welcome!</p>`);

                }

                answerTheQuestion(student, question) {
                        chat.innerHTML += (`<p><span class="bold">${student.name}</span> ask from <span class="bold">${this.name}</span>: ${question}</p>`);

                        if (student.type !== 'monitor') {
                                chat.innerHTML += (`<p class="error"><span class="bold">${this.name}</span>: "${question}" - it is not your bussines!</p>`);
                        } else {
                                chat.innerHTML += (`<p class="green"><span class="bold">${this.name}</span>: Yes, my dear?!</p>`);
                        }
                }
        }

        class Student {
                constructor(name) {
                        this.name = name;
                        this.monitor = null;
                        this.type = 'student';

                        info.innerHTML += (`<p>New student <span class="bold"> ${this.name} </span>! Welcome!</p>`);
                }

                getAnswer(message, from) {
                        chat.innerHTML += (`<p><span class="bold">${from.name}</span> tells <span class="bold">${this.name}</span>: ${message}</p>`);
                }

                tipTheMonitor(message, to) {
                        if (this.monitor !== null) {
                                if (to !== undefined) {
                                        if (to.type === 'professor') {
                                                to.answerTheQuestion(this, message);
                                        } else if (to !== undefined) {
                                                chat.innerHTML += (`<p><span class="bold">${this.name}</span> tells <span class="bold">${to.name}</span>: ${message}</p>`);
                                        }
                                } else {
                                        chat.innerHTML += (`<p><span class="bold">${this.name}</span> tells to everyone: ${message}</p>`);
                                }
                        } else {
                                chat.innerHTML += (`<p class="warn"><span class="bold">${this.name}</span> - you are not in group. Ask the monitor to add you to the group!</p>`)
                        }
                }
        }

        // Monitor == Староста
        class Monitor extends Student {
                constructor(name) {
                        super(name);
                        this.name = name;
                        this.group = {};
                        this.type = 'monitor';
                }

                addToGroup(student) {
                        this.group[student.name] = student;
                        student.monitor = this;
                        info.innerHTML += (`<p>Student <span class="bold"> ${student.name} </span> has been added to the chat</p>`);
                        list.innerHTML += `<p>${student.name}</p>`
                }

                askProfessor(message, from, to) {
                        to.answerTheQuestion(this, message, to, from);
                }
        }

        // add students
        const student_1 = new Student('Nadia');
        const student_2 = new Student('Serhii');
        const student_3 = new Student('Vlad');
        const student_4 = new Student('Looser');

        // add professors
        const professor_1 = new Professor('Antonina Stepanovna');
        const professor_2 = new Professor('Boris Albertovich');

        // add monitor
        const monitor = new Monitor('Klava');

        // added students to group
        monitor.addToGroup(monitor);
        monitor.addToGroup(student_1);
        monitor.addToGroup(student_2);
        monitor.addToGroup(student_3);

        // test
        student_4.tipTheMonitor('Hi everyone!'); // get error
        student_3.tipTheMonitor('Hi everyone!');
        student_2.tipTheMonitor('Hi!', student_3);
        student_2.tipTheMonitor('Where are you?', professor_2);
        monitor.tipTheMonitor('Where are you?', professor_2);

        professor_1.answerTheQuestion(student_2, 'Where are you?');
        professor_1.answerTheQuestion(monitor, 'Where are you?');


        student_1.getAnswer('Hi!', professor_1);

        student_1.tipTheMonitor('Do we have hometask for tommorow lesson?', professor_2);
        monitor.askProfessor('Do we have hometask for tommorow lesson?', student_3, professor_2);
};

export default Mediator;
