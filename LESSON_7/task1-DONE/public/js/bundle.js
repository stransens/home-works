/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/index.js":
/*!******************************!*\
  !*** ./application/index.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _mediator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./mediator */ \"./application/mediator.js\");\n\n\nObject(_mediator__WEBPACK_IMPORTED_MODULE_0__[\"default\"])();\n\n\n//# sourceURL=webpack:///./application/index.js?");

/***/ }),

/***/ "./application/mediator.js":
/*!*********************************!*\
  !*** ./application/mediator.js ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/*\n  Написать медиатор для группы студентов.\n\n  Профессор отвечает только на вопросы старосты.\n\n  У Студента есть имя и группа которой он принадлежит.\n  Он может запросить старосту задать вопрос или получить ответ.\n\n  Староста может добавлять студентов в группу и передавать вопрос профессору.\n*/\n\n\n\n//**************************************** OPEN INDEX.HTML IN BROWSER\n\nconst chat = document.getElementById('chat');\nconst info = document.getElementById('info');\nconst list = document.getElementById('list');\nconst Mediator = () => {\n\n        class Professor {\n                constructor(name) {\n                        this.name = name;\n                        this.type = 'professor';\n\n                        info.innerHTML += (`<p>New professor <span class=\"bold\"> ${this.name} </span>! Welcome!</p>`);\n\n                }\n\n                answerTheQuestion(student, question) {\n                        chat.innerHTML += (`<p><span class=\"bold\">${student.name}</span> ask from <span class=\"bold\">${this.name}</span>: ${question}</p>`);\n\n                        if (student.type !== 'monitor') {\n                                chat.innerHTML += (`<p class=\"error\"><span class=\"bold\">${this.name}</span>: \"${question}\" - it is not your bussines!</p>`);\n                        } else {\n                                chat.innerHTML += (`<p class=\"green\"><span class=\"bold\">${this.name}</span>: Yes, my dear?!</p>`);\n                        }\n                }\n        }\n\n        class Student {\n                constructor(name) {\n                        this.name = name;\n                        this.monitor = null;\n                        this.type = 'student';\n\n                        info.innerHTML += (`<p>New student <span class=\"bold\"> ${this.name} </span>! Welcome!</p>`);\n                }\n\n                getAnswer(message, from) {\n                        chat.innerHTML += (`<p><span class=\"bold\">${from.name}</span> tells <span class=\"bold\">${this.name}</span>: ${message}</p>`);\n                }\n\n                tipTheMonitor(message, to) {\n                        if (this.monitor !== null) {\n                                if (to !== undefined) {\n                                        if (to.type === 'professor') {\n                                                to.answerTheQuestion(this, message);\n                                        } else if (to !== undefined) {\n                                                chat.innerHTML += (`<p><span class=\"bold\">${this.name}</span> tells <span class=\"bold\">${to.name}</span>: ${message}</p>`);\n                                        }\n                                } else {\n                                        chat.innerHTML += (`<p><span class=\"bold\">${this.name}</span> tells to everyone: ${message}</p>`);\n                                }\n                        } else {\n                                chat.innerHTML += (`<p class=\"warn\"><span class=\"bold\">${this.name}</span> - you are not in group. Ask the monitor to add you to the group!</p>`)\n                        }\n                }\n        }\n\n        // Monitor == Староста\n        class Monitor extends Student {\n                constructor(name) {\n                        super(name);\n                        this.name = name;\n                        this.group = {};\n                        this.type = 'monitor';\n                }\n\n                addToGroup(student) {\n                        this.group[student.name] = student;\n                        student.monitor = this;\n                        info.innerHTML += (`<p>Student <span class=\"bold\"> ${student.name} </span> has been added to the chat</p>`);\n                        list.innerHTML += `<p>${student.name}</p>`\n                }\n\n                askProfessor(message, from, to) {\n                        to.answerTheQuestion(this, message, to, from);\n                }\n        }\n\n        // add students\n        const student_1 = new Student('Nadia');\n        const student_2 = new Student('Serhii');\n        const student_3 = new Student('Vlad');\n        const student_4 = new Student('Looser');\n\n        // add professors\n        const professor_1 = new Professor('Antonina Stepanovna');\n        const professor_2 = new Professor('Boris Albertovich');\n\n        // add monitor\n        const monitor = new Monitor('Klava');\n\n        // added students to group\n        monitor.addToGroup(monitor);\n        monitor.addToGroup(student_1);\n        monitor.addToGroup(student_2);\n        monitor.addToGroup(student_3);\n\n        // test\n        student_4.tipTheMonitor('Hi everyone!'); // get error\n        student_3.tipTheMonitor('Hi everyone!');\n        student_2.tipTheMonitor('Hi!', student_3);\n        student_2.tipTheMonitor('Where are you?', professor_2);\n        monitor.tipTheMonitor('Where are you?', professor_2);\n\n        professor_1.answerTheQuestion(student_2, 'Where are you?');\n        professor_1.answerTheQuestion(monitor, 'Where are you?');\n\n\n        student_1.getAnswer('Hi!', professor_1);\n\n        student_1.tipTheMonitor('Do we have hometask for tommorow lesson?', professor_2);\n        monitor.askProfessor('Do we have hometask for tommorow lesson?', student_3, professor_2);\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Mediator);\n\n\n//# sourceURL=webpack:///./application/mediator.js?");

/***/ })

/******/ });