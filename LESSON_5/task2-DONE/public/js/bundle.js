/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/index.js":
/*!******************************!*\
  !*** ./application/index.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _observer__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./observer */ \"./application/observer.js\");\n\n\nObject(_observer__WEBPACK_IMPORTED_MODULE_0__[\"default\"])();\n\n\n\n//# sourceURL=webpack:///./application/index.js?");

/***/ }),

/***/ "./application/observer.js":
/*!*********************************!*\
  !*** ./application/observer.js ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/*\n  Задание: Модуль создания плейлиста, используя паттерн Обсервер.\n\n  У вас есть данные о исполнителях и песнях. Задание делится на три модуля:\n    1. Список исполнителей и песен (Находится слева) - отуда можно включить\n    песню в исполнение иди добавить в плейлист.\n    Если песня уже есть в плейлисте, дважды добавить её нельзя.\n\n    2. Плейлист (Находится справа) - список выбраных песен, песню можно удалить,\n    или запустить в исполнение. Внизу списка должен выводиться блок, в котором\n    пишет суммарное время проигрывания всех песен в плейлисте.\n\n    3. Отображает песню которая проигрывается.\n\n    4. + Бонус: Сделать прогресс пар того как проигрывается песня\n    с возможностью его остановки.\n*/\n\nconst MusicList = [\n        {\n                title: 'Rammstain',\n                songs: [\n                        {\n                                id: 1,\n                                name: 'Du Hast',\n                                time: [3, 12]\n                        },\n                        {\n                                id: 2,\n                                name: 'Ich Will',\n                                time: [5, 1]\n                        },\n                        {\n                                id: 3,\n                                name: 'Mutter',\n                                time: [4, 15]\n                        },\n                        {\n                                id: 4,\n                                name: 'Ich tu dir weh',\n                                time: [5, 13]\n                        },\n                        {\n                                id: 5,\n                                name: 'Rammstain',\n                                time: [3, 64]\n                        }\n                ]\n        },\n        {\n                title: 'System of a Down',\n                songs: [\n                        {\n                                id: 6,\n                                name: 'Toxicity',\n                                time: [4, 22]\n                        },\n                        {\n                                id: 7,\n                                name: 'Sugar',\n                                time: [2, 44]\n                        },\n                        {\n                                id: 8,\n                                name: 'Lonely Day',\n                                time: [3, 19]\n                        },\n                        {\n                                id: 9,\n                                name: 'Lost in Hollywood',\n                                time: [5, 9]\n                        },\n                        {\n                                id: 10,\n                                name: 'Chop Suey!',\n                                time: [2, 57]\n                        }\n                ]\n        },\n        {\n                title: 'Green Day',\n                songs: [\n                        {\n                                id: 11,\n                                name: '21 Guns',\n                                time: [4, 16]\n                        },\n                        {\n                                id: 12,\n                                name: 'Boulevard of broken dreams!',\n                                time: [6, 37]\n                        },\n                        {\n                                id: 13,\n                                name: 'Basket Case!',\n                                time: [3, 21]\n                        },\n                        {\n                                id: 14,\n                                name: 'Know Your Enemy',\n                                time: [4, 11]\n                        }\n                ]\n        },\n        {\n                title: 'Linkin Park',\n                songs: [\n                        {\n                                id: 15,\n                                name: 'Numb',\n                                time: [3, 11]\n                        },\n                        {\n                                id: 16,\n                                name: 'New Divide',\n                                time: [4, 41]\n                        },\n                        {\n                                id: 17,\n                                name: 'Breaking the Habit',\n                                time: [4, 1]\n                        },\n                        {\n                                id: 18,\n                                name: 'Faint',\n                                time: [3, 29]\n                        }\n                ]\n        }\n];\n\nfunction Observable() {\n        let observers = [];\n        this.sendMessage = function (msg) {\n                observers.map((obs) => {\n                        obs.notify(msg);\n                });\n        };\n        this.sendMessage = function (msg) {\n                observers.map((obs) => {\n                        obs.notify(msg);\n                });\n        };\n        this.addObserver = function (observer) {\n                observers.push(observer);\n        };\n\n}\n\nfunction Observer(behavior) {\n        this.notify = function (callback) {\n                behavior(callback);\n        };\n}\n\nconst MusicBox = () => {\n        let MusicPlaying = document.getElementById('MusicPlaying');\n        let MusicPlayList = document.getElementById('MusicPlayList');\n        let playlist = [];\n\n        let observable = new Observable();\n        let observerAddToPL = new Observer(id => {\n                MusicList.map(Artist => {\n                        Artist.songs.map(song => {\n                                if (Number(song.id) === Number(id)) {\n                                        playlist.push(song);\n                                }\n                        });\n                });\n                player.playlistRender();\n        });\n\n        observable.addObserver(observerAddToPL);\n\n        class Player {\n                constructor() {\n                        this.render = this.render.bind(this);\n                        this.playlistRender = this.playlistRender.bind(this);\n                        this.activeRender = this.activeRender.bind(this);\n                        this.removeSong = this.removeSong.bind(this);\n                }\n\n                render() {\n                        const MusicBox = document.getElementById('MusicBox');\n                        MusicList.map(Artist => {\n                                const div = document.createElement('div');\n                                div.innerHTML = `<h4>${Artist.title}</h4>`;\n                                Artist.songs.map(song => {\n                                        const ul = document.createElement('ul');\n                                        ul.innerHTML += `<li>${song.name} <i class=\"play fas fa-play\" data-id=\"${song.id}\"></i><i class=\"fas fa-plus add\" data-id=\"${song.id}\"></i></li>`;\n                                        div.appendChild(ul);\n                                        let play = div.querySelector(`.play[data-id=\"${song.id}\"]`);\n                                        play.addEventListener('click', this.activeRender);\n                                        let add = div.querySelector(`.add[data-id=\"${song.id}\"]`);\n                                        add.addEventListener('click', e => {\n                                                let id = e.target.dataset.id;\n                                                observable.sendMessage(id);\n                                                add.setAttribute('style', 'pointer-events: none;')\n                                        });\n                                });\n                                MusicBox.appendChild(div);\n                        });\n                }\n\n\n                playlistRender() {\n                        let duration = 0;\n                        let time = [0, 0];\n\n                        MusicPlayList.innerHTML = '';\n\n                        playlist.map(song => {\n                                let block = document.createElement('div');\n                                block.innerHTML = `<b>${song.name}</b>, ${song.time[0]}:${song.time[1]} <i class=\"fas fa-minus-circle remove\" data-id='${song.id}'></i><br />`;\n                                MusicPlayList.appendChild(block);\n                                let remove = document.querySelector(`.remove[data-id=\"${song.id}\"]`);\n                                remove.addEventListener('click', this.removeSong);\n                                duration += (Number(song.time[0]) * 60) + Number(song.time[1]);\n                        });\n\n                        if (playlist.length !== 0) {\n                                let par = document.createElement('p');\n                                time[0] = Math.floor(duration / 60);\n                                time[1] = Math.floor(duration - time[0] * 60);\n                                par.innerHTML = `Продолжительность: ${time[0]}:${time[1]}`;\n                                MusicPlayList.appendChild(par);\n                        }\n                }\n\n                removeSong(e) {\n                        let id = e.target.dataset.id;\n                        playlist.map(song => {\n                                if (Number(song.id) === Number(id)) {\n                                        let idx = playlist.indexOf(song);\n                                        playlist.splice(idx, 1);\n                                }\n                        });\n                        let leftList = document.getElementsByClassName('add');\n                        console.log(leftList);\n                        let leftArray = Array.from(leftList);\n                        console.log(leftArray);\n                        leftArray.forEach(item => {\n                                if (item.dataset.id === id) {\n                                        item.removeAttribute('style');\n                                }\n                        })\n                        this.playlistRender();\n                }\n\n                activeRender(e) {\n                        let id = e.target.dataset.id;\n                        MusicList.map(Artist => {\n                                Artist.songs.map(song => {\n                                        if (Number(id) === Number(song.id)) {\n                                                MusicPlaying.innerHTML =\n                                                        `\n                                                              <div class=\"song__name\">${song.name}</div>\n                                                              <div class=\"song__creator\">${Artist.title}</div>\n                                                              <div class=\"song__duration\">${song.time[0]}:${song.time[1]}</div>\n                                                              <div class=\"song__duration\" id=\"song_progress\"></div>\n                                                              <button id='pause' data-status=\"playing\">&#10074;&#10074;</button>           \n                                                        `;\n                                                let btn = document.getElementById('pause');\n                                                btn.addEventListener('click', e => {\n                                                        if (e.target.dataset.status === 'playing') {\n                                                                btn.innerHTML = '&#x25b6;';\n                                                                e.target.dataset.status = 'paused';\n                                                                if (MusicPlaying.timer) {\n                                                                        clearInterval(MusicPlaying.timer);\n                                                                }\n                                                        } else if (e.target.dataset.status === 'paused') {\n                                                                btn.innerHTML = '&#10074;&#10074;';\n                                                                e.target.dataset.status = 'playing';\n                                                                progressbar.start();\n                                                        }\n\n                                                });\n                                                MusicPlaying.appendChild(btn);\n                                                let progressbar = new progressBar(song.time[0], song.time[1]);\n                                                progressbar.start();\n                                        }\n                                });\n                        });\n                }\n        }\n\n        let player = new Player;\n        player.render();\n\n        class progressBar {\n                constructor(min, sec) {\n                        this.length = (Number(min) * 60) + Number(sec);\n                        this.value = this.length;\n                        this.start = this.start.bind(this);\n                        this.render = this.render.bind(this);\n                }\n\n                render() {\n                        let block = document.getElementById('song_progress');\n                        let template =\n                                `\n                                  <progress max='${this.length}' value='${this.value}'>\n                                  </progress>\n                        `\n                        block.innerHTML = template;\n                }\n\n                start() {\n                        if (MusicPlaying.timer) {\n                                clearInterval(MusicPlaying.timer);\n                        }\n                        MusicPlaying.timer = setInterval(() => {\n                                if (this.value !== 0) {\n                                        this.value--\n                                } else if (this.value === 0) {\n                                        clearInterval(MusicPlaying.timer)\n                                }\n                                this.render();\n                        }, 1000)\n\n                }\n        }\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (MusicBox);\n\n\n//# sourceURL=webpack:///./application/observer.js?");

/***/ })

/******/ });