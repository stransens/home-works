/*
  Задание: Модуль создания плейлиста, используя паттерн Обсервер.

  У вас есть данные о исполнителях и песнях. Задание делится на три модуля:
    1. Список исполнителей и песен (Находится слева) - отуда можно включить
    песню в исполнение иди добавить в плейлист.
    Если песня уже есть в плейлисте, дважды добавить её нельзя.

    2. Плейлист (Находится справа) - список выбраных песен, песню можно удалить,
    или запустить в исполнение. Внизу списка должен выводиться блок, в котором
    пишет суммарное время проигрывания всех песен в плейлисте.

    3. Отображает песню которая проигрывается.

    4. + Бонус: Сделать прогресс пар того как проигрывается песня
    с возможностью его остановки.
*/

const MusicList = [
        {
                title: 'Rammstain',
                songs: [
                        {
                                id: 1,
                                name: 'Du Hast',
                                time: [3, 12]
                        },
                        {
                                id: 2,
                                name: 'Ich Will',
                                time: [5, 1]
                        },
                        {
                                id: 3,
                                name: 'Mutter',
                                time: [4, 15]
                        },
                        {
                                id: 4,
                                name: 'Ich tu dir weh',
                                time: [5, 13]
                        },
                        {
                                id: 5,
                                name: 'Rammstain',
                                time: [3, 64]
                        }
                ]
        },
        {
                title: 'System of a Down',
                songs: [
                        {
                                id: 6,
                                name: 'Toxicity',
                                time: [4, 22]
                        },
                        {
                                id: 7,
                                name: 'Sugar',
                                time: [2, 44]
                        },
                        {
                                id: 8,
                                name: 'Lonely Day',
                                time: [3, 19]
                        },
                        {
                                id: 9,
                                name: 'Lost in Hollywood',
                                time: [5, 9]
                        },
                        {
                                id: 10,
                                name: 'Chop Suey!',
                                time: [2, 57]
                        }
                ]
        },
        {
                title: 'Green Day',
                songs: [
                        {
                                id: 11,
                                name: '21 Guns',
                                time: [4, 16]
                        },
                        {
                                id: 12,
                                name: 'Boulevard of broken dreams!',
                                time: [6, 37]
                        },
                        {
                                id: 13,
                                name: 'Basket Case!',
                                time: [3, 21]
                        },
                        {
                                id: 14,
                                name: 'Know Your Enemy',
                                time: [4, 11]
                        }
                ]
        },
        {
                title: 'Linkin Park',
                songs: [
                        {
                                id: 15,
                                name: 'Numb',
                                time: [3, 11]
                        },
                        {
                                id: 16,
                                name: 'New Divide',
                                time: [4, 41]
                        },
                        {
                                id: 17,
                                name: 'Breaking the Habit',
                                time: [4, 1]
                        },
                        {
                                id: 18,
                                name: 'Faint',
                                time: [3, 29]
                        }
                ]
        }
];

function Observable() {
        let observers = [];
        this.sendMessage = function (msg) {
                observers.map((obs) => {
                        obs.notify(msg);
                });
        };
        this.sendMessage = function (msg) {
                observers.map((obs) => {
                        obs.notify(msg);
                });
        };
        this.addObserver = function (observer) {
                observers.push(observer);
        };

}

function Observer(behavior) {
        this.notify = function (callback) {
                behavior(callback);
        };
}

const MusicBox = () => {
        let MusicPlaying = document.getElementById('MusicPlaying');
        let MusicPlayList = document.getElementById('MusicPlayList');
        let playlist = [];

        let observable = new Observable();
        let observerAddToPL = new Observer(id => {
                MusicList.map(Artist => {
                        Artist.songs.map(song => {
                                if (Number(song.id) === Number(id)) {
                                        playlist.push(song);
                                }
                        });
                });
                player.playlistRender();
        });

        observable.addObserver(observerAddToPL);

        class Player {
                constructor() {
                        this.render = this.render.bind(this);
                        this.playlistRender = this.playlistRender.bind(this);
                        this.activeRender = this.activeRender.bind(this);
                        this.removeSong = this.removeSong.bind(this);
                }

                render() {
                        const MusicBox = document.getElementById('MusicBox');
                        MusicList.map(Artist => {
                                const div = document.createElement('div');
                                div.innerHTML = `<h4>${Artist.title}</h4>`;
                                Artist.songs.map(song => {
                                        const ul = document.createElement('ul');
                                        ul.innerHTML += `<li>${song.name} <i class="play fas fa-play" data-id="${song.id}"></i><i class="fas fa-plus add" data-id="${song.id}"></i></li>`;
                                        div.appendChild(ul);
                                        let play = div.querySelector(`.play[data-id="${song.id}"]`);
                                        play.addEventListener('click', this.activeRender);
                                        let add = div.querySelector(`.add[data-id="${song.id}"]`);
                                        add.addEventListener('click', e => {
                                                let id = e.target.dataset.id;
                                                observable.sendMessage(id);
                                                add.setAttribute('style', 'pointer-events: none;')
                                        });
                                });
                                MusicBox.appendChild(div);
                        });
                }


                playlistRender() {
                        let duration = 0;
                        let time = [0, 0];

                        MusicPlayList.innerHTML = '';

                        playlist.map(song => {
                                let block = document.createElement('div');
                                block.innerHTML = `<b>${song.name}</b>, ${song.time[0]}:${song.time[1]} <i class="fas fa-minus-circle remove" data-id='${song.id}'></i><br />`;
                                MusicPlayList.appendChild(block);
                                let remove = document.querySelector(`.remove[data-id="${song.id}"]`);
                                remove.addEventListener('click', this.removeSong);
                                duration += (Number(song.time[0]) * 60) + Number(song.time[1]);
                        });

                        if (playlist.length !== 0) {
                                let par = document.createElement('p');
                                time[0] = Math.floor(duration / 60);
                                time[1] = Math.floor(duration - time[0] * 60);
                                par.innerHTML = `Продолжительность: ${time[0]}:${time[1]}`;
                                MusicPlayList.appendChild(par);
                        }
                }

                removeSong(e) {
                        let id = e.target.dataset.id;
                        playlist.map(song => {
                                if (Number(song.id) === Number(id)) {
                                        let idx = playlist.indexOf(song);
                                        playlist.splice(idx, 1);
                                }
                        });
                        let leftList = document.getElementsByClassName('add');
                        console.log(leftList);
                        let leftArray = Array.from(leftList);
                        console.log(leftArray);
                        leftArray.forEach(item => {
                                if (item.dataset.id === id) {
                                        item.removeAttribute('style');
                                }
                        })
                        this.playlistRender();
                }

                activeRender(e) {
                        let id = e.target.dataset.id;
                        MusicList.map(Artist => {
                                Artist.songs.map(song => {
                                        if (Number(id) === Number(song.id)) {
                                                MusicPlaying.innerHTML =
                                                        `
                                                              <div class="song__name">${song.name}</div>
                                                              <div class="song__creator">${Artist.title}</div>
                                                              <div class="song__duration">${song.time[0]}:${song.time[1]}</div>
                                                              <div class="song__duration" id="song_progress"></div>
                                                              <button id='pause' data-status="playing">&#10074;&#10074;</button>           
                                                        `;
                                                let btn = document.getElementById('pause');
                                                btn.addEventListener('click', e => {
                                                        if (e.target.dataset.status === 'playing') {
                                                                btn.innerHTML = '&#x25b6;';
                                                                e.target.dataset.status = 'paused';
                                                                if (MusicPlaying.timer) {
                                                                        clearInterval(MusicPlaying.timer);
                                                                }
                                                        } else if (e.target.dataset.status === 'paused') {
                                                                btn.innerHTML = '&#10074;&#10074;';
                                                                e.target.dataset.status = 'playing';
                                                                progressbar.start();
                                                        }

                                                });
                                                MusicPlaying.appendChild(btn);
                                                let progressbar = new progressBar(song.time[0], song.time[1]);
                                                progressbar.start();
                                        }
                                });
                        });
                }
        }

        let player = new Player;
        player.render();

        class progressBar {
                constructor(min, sec) {
                        this.length = (Number(min) * 60) + Number(sec);
                        this.value = this.length;
                        this.start = this.start.bind(this);
                        this.render = this.render.bind(this);
                }

                render() {
                        let block = document.getElementById('song_progress');
                        let template =
                                `
                                  <progress max='${this.length}' value='${this.value}'>
                                  </progress>
                        `
                        block.innerHTML = template;
                }

                start() {
                        if (MusicPlaying.timer) {
                                clearInterval(MusicPlaying.timer);
                        }
                        MusicPlaying.timer = setInterval(() => {
                                if (this.value !== 0) {
                                        this.value--
                                } else if (this.value === 0) {
                                        clearInterval(MusicPlaying.timer)
                                }
                                this.render();
                        }, 1000)

                }
        }
};

export default MusicBox;
