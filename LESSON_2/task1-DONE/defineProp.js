/*
  Задание:

  Написать класс SuperDude который как аргумент принимает два параметра:
    - Имя
    - Массив суперспособностей которые являются обьектом.

  Object.defineProperty(obj, prop, descriptor)

    Модель суперспособности:
      {
        // Имя способности
        name:'Invisibility',
        // Сообщение которое будет выведено когда способность была вызвана
        spell: function(){ return `${this.name} hide from you`}
      }

    В конструкторе, нужно:
    - сделать так, что бы имя нельзя было перезаписывать и присвоить ему то
      значение которое мы передали как аргумент.

    - перебрать массив способностей и на каждую из них создать метод для этого
      обьекта, используя поле name как название метода, а spell как то,
      что нужно вернуть в console.log при вызове этого метода.
    - все способности должны быть неизменяемые

    - бонус, создать конструктор суперспособностей -> new Spell( name, spellFunc );
*/

class SuperDude {
        constructor(name) {
                Object.defineProperty(
                        this,
                        'name',
                        {
                                value: name,
                        });
                this.addMethod = function (powers) {
                        for (let i = 0; i < powers.length; i++) {
                                Object.defineProperty(
                                        this,
                                        powers[i].name,
                                        {
                                                value: powers[i].spell,
                                        });
                        }
                }
        }
}

class Dude extends SuperDude {
        constructor(name, powers) {
                super(name, powers);
        }
}

let superPowers = [
        {
                name: 'Invisibility', spell: function () {
                        console.log(`${this.name} hide from you`)
                }
        },
        {
                name: 'superSpeed', spell: function () {
                        console.log(`${this.name} running from you`)
                }
        },
        {
                name: 'superSight', spell: function () {
                        console.log(`${this.name} see you`)
                }
        },
        {
                name: 'superFroze', spell: function () {
                        console.log(`${this.name} will froze you`)
                }
        },
        {
                name: 'superSkin', spell: function () {
                        console.log(`${this.name} skin is unbreakable`)
                }
        },
]

let Luther = new Dude('Luther', superPowers);
Luther.addMethod(superPowers);
console.log(Luther);

// Тестирование: Методы должны работать и выводить сообщение.
Luther.superSight();
Luther.superSpeed();
Luther.superFroze();
Luther.Invisibility();
Luther.superSkin();

class Spell {
        constructor(name, spellText) {
                this.name = name;
                this.spell = function () {
                        console.log(`${this.name} ${spellText}`)
                };
        }
}

// BONUS!!!!!
let newSpell = new Spell('newSpell', 'получил новую способность!!');
let newSpell2 = new Spell('newSpell2', 'получил еще одну!!! ');
let arrayNewPowers = [];
arrayNewPowers.push(newSpell);
arrayNewPowers.push(newSpell2);
Luther.addMethod(arrayNewPowers);

Luther.newSpell();
Luther.newSpell2();

