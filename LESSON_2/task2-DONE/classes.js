/*
  Задание:

    I. Написать класс Post который будет уметь:

      1. Конструктор:
          title
          image
          description
          likes

      2. Методы
          render -> отрисовать элемент в ленте
          likePost -> увеличивает счетчик постов
          Методы для изменения title, image, description
          + бонус. Сделать получение и изменение через set и get

    II. Написать класс Advertisment который будет экстендится от класа Post
        но помимо будет иметь другой шаблон вывода, а так же метод для покупки обьекта

        buyItem -> выведет сообщение что вы купили обьект

    III.  Сгенерировать ленту из всех постов что вы добавили в систему.
          Каждый третий пост должен быть рекламным

    <div class="post">
      <div class="post__title">Some post</div>
      <div class="post__image">
        <img src="https://cdn-images-1.medium.com/fit/t/1600/480/1*Se0gnNo-tsQG-1jeu_4TJw.png"/>
      </div>
      <div class="post__description"></div>
      <div class="post__footer">
        <button class="post__like">Like!</button>
      </div>
    </div>
*/

class Post {
        constructor({title, image, description, likes, id}) {
                this._title = title;
                this._image = image;
                this._description = description;
                this.likes = likes;
                this.id = id;
        }

        likePost() {
                this.likes++
        }

        render() {

                let feed = document.createElement('div');
                feed.classList.add('post-wrapper');

                let template = `
                         <div class="post">
                              <div class="post__title">${this._title}</div>
                              <div class="post__image">
                                <img src="${this._image}"/>
                              </div>
                              <div class="post__description">${this._description}</div>
                              <div class="post__footer">
                                <button class="post__like" id="${this.id}">Like! (${this.likes})</button>
                              </div>
                        </div>
                `;

                feed.innerHTML += template;

                let btn = feed.querySelector(`.post__like`);

                btn.addEventListener('click', () =>{
                        this.likePost();
                        btn.innerHTML = `Like! (${this.likes})`;
                });

                return feed;
        }

        get title() {
                return this._title = this._title;
        }

        set title(newTitle){
                this._title = newTitle;
        }

        get image() {
                return this._image = `<img src="${this._image}">`;
        }

        set image(newImg){
                this._image = newImg;
        }

        get description() {
                return this._description = `Your result is: ${this._description}`;
        }

        set description (newDescr){
                this._description = newDescr;
        }


}

class Advertisment extends Post {
        constructor(props) {
                super(props);
                this.buyItem = this.buyItem.bind(props);
                this.render = this.render.bind(this);
        }

        buyItem() {
                console.log(`You buy ${this.title}`)
        }

        render() {
                let div = document.createElement('div');
                div.classList.add('post-wrapper');

                let template = `
                         <div class="post adver">
                              <div class="post__title">${this._title}</div>
                              <div class="post__image">
                                <img src="${this._image}"/>
                              </div>
                              <div class="post__description">${this._description}</div>
                              <div class="post__footer">
                                <button class="post__like" id="post${this.id}">Buy!</button>
                              </div>
                        </div>
                `;

                div.innerHTML = template;
                let btn = div.querySelector(`.post__like`);
                btn.addEventListener('click', this.buyItem);

                return div;
        }
}

let content = [
        {
                title: 'Post #1',
                image: 'https://cdn-images-1.medium.com/fit/t/1600/480/1*Se0gnNo-tsQG-1jeu_4TJw.png',
                description: 'Description for post',
                likes: 0,
                id: 1,
        },
        {
                title: 'Post #2',
                image: 'https://cdn-images-1.medium.com/fit/t/1600/480/1*Se0gnNo-tsQG-1jeu_4TJw.png',
                description: 'Description for post',
                likes: 0,
                id: 2,
        },
        {
                title: 'Post #3',
                image: 'https://cdn-images-1.medium.com/fit/t/1600/480/1*Se0gnNo-tsQG-1jeu_4TJw.png',
                description: 'Description for post',
                likes: 0,
                id: 3,
        },
        {
                title: 'Post #4',
                image: 'https://cdn-images-1.medium.com/fit/t/1600/480/1*Se0gnNo-tsQG-1jeu_4TJw.png',
                description: 'Description for post',
                likes: 0,
                id: 4,
        },
        {
                title: 'Post #5',
                image: 'https://cdn-images-1.medium.com/fit/t/1600/480/1*Se0gnNo-tsQG-1jeu_4TJw.png',
                description: 'Description for post',
                likes: 0,
                id: 5,
        },
        {
                title: 'Post #6',
                image: 'https://cdn-images-1.medium.com/fit/t/1600/480/1*Se0gnNo-tsQG-1jeu_4TJw.png',
                description: 'Description for post',
                likes: 0,
                id: 6,
        },
        {
                title: 'Post #7',
                image: 'https://cdn-images-1.medium.com/fit/t/1600/480/1*Se0gnNo-tsQG-1jeu_4TJw.png',
                description: 'Description for post',
                likes: 0,
                id: 7,
        },
        {
                title: 'Post #8',
                image: 'https://cdn-images-1.medium.com/fit/t/1600/480/1*Se0gnNo-tsQG-1jeu_4TJw.png',
                description: 'Description for post',
                likes: 0,
                id: 8,
        },
        {
                title: 'Post #9',
                image: 'https://cdn-images-1.medium.com/fit/t/1600/480/1*Se0gnNo-tsQG-1jeu_4TJw.png',
                description: 'Description for post',
                likes: 0,
                id: 9,
        },
        {
                title: 'Post #10',
                image: 'https://cdn-images-1.medium.com/fit/t/1600/480/1*Se0gnNo-tsQG-1jeu_4TJw.png',
                description: 'Description for post',
                likes: 0,
                id: 10,
        },
        {
                title: 'Post #11',
                image: 'https://cdn-images-1.medium.com/fit/t/1600/480/1*Se0gnNo-tsQG-1jeu_4TJw.png',
                description: 'Description for post',
                likes: 0,
                id: 11,
        }
];

let feedList = document.getElementById('posts_feed');

content.forEach((item, i) => {
        if ((i + 1) % 3) {
                let newPost = new Post(item);
                newPost._title = 'Another title';
                feedList.appendChild(newPost.render());
        } else {
                let newPost = new Advertisment(item);
                newPost._title = 'РЕКЛАМА!!!';
                feedList.appendChild(newPost.render());
        }
});